<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recipe".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $ingredients
 * @property string $image
 * @property int $category
 * @property string $cooking_time
 * @property string $tags
 * @property int $posted_by
 * @property string $created_date
 */
class Recipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'ingredients', 'image', 'category', 'cooking_time', 'posted_by', 'created_date'], 'required'],
            [['description', 'ingredients', 'image', 'tags'], 'string'],
            [['category', 'posted_by'], 'integer'],
            [['created_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['cooking_time'], 'string', 'max' => 10],
          //  [['file'], 'file', 'extensions'=>'jpg, gif, png, jpeg'],
            //[['file'], 'file', 'maxSize'=>'100000'],

    
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'ingredients' => Yii::t('app', 'Ingredients'),
            'image' => Yii::t('app', 'Image'),
            'category' => Yii::t('app', 'Category'),
            'cooking_time' => Yii::t('app', 'Cooking Time'),
            'tags' => Yii::t('app', 'Tags'),
            'posted_by' => Yii::t('app', 'Posted By'),
            'created_date' => Yii::t('app', 'Created Date'),
        ];
    }
    /*
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    */

    public function getCategory1()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    } 


    public function getPostedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'posted_by']);
    }
    
}
