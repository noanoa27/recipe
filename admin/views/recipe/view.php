<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Recipe */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recipe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'ingredients:ntext',
           // 'image:ntext',
           // 'category',
           [                      
            'label' => 'Category',
            'value' => $model->category1->name,
        ], 

            'cooking_time',
            'tags',
            [                      
                'label' => 'Posted By',
                'format' => 'html',
                'value' => Html::a($model->postedBy->username, 
                    ['user/view', 'id' => $model->postedBy->id]),                
            ],
          
            'created_date',
        ],
    ]) ?>

</div>
