<?php

use yii\db\Migration;

/**
 * Handles the creation of table `recipe`.
 */
class m180923_192752_create_recipe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
			$this->createTable('review', [
			'id' => $this->primaryKey(),
			'name' => $this->integer()->notNull(), 
			'description' => $this->text()->notNull(),
			'ingredients' => $this->text()->notNull(),
			'image' => $this->text()->notNull(),
			'cooking_time' => $this->string()->notNull(),
			'posted_by' => $this->integer()->notNull(),
			'created_date' => $this->date()->notNull(),

			]);
			$this->addForeignKey(
			'category',
			);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('recipe');
    }
}
