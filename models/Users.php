<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $password
 * @property string $role
 * @property string $authKey
 * @property string $accessToken
 * @property int $status
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'password',], 'required'],
            //[['password', 'status'], 'integer'],
            //[['role'], 'string'],
            [['first_name', 'last_name', 'email',], 'string', 'max' => 30],
            ['email', 'email'],
            [['password',], 'string', 'min' => 8,'tooShort'=>'Password should contain at least 8 characters.'],
            [['email'], 'unique','message' => 'Email already exist.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'role' => 'Role',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'status' => 'Status',
        ];
    }
    public function generatePassword($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function getAuthKey()
    {
        return $this->authKey;
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    public function getUsername()
    {
        return $this->email;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return self::findOne(['accessToken' => $token]);
    }
    public static function findByUsername($username)
    {
        return self::findOne(['email'=>$username]);
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //echo $password;exit;
       // echo $this->AssignedPW."<br>".$password;exit;
        //return $this->AssignedPW === $password;
        if(Yii::$app->getSecurity()->validatePassword($password, $this->password))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
}
